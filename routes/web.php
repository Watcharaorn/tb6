<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', function () {
    return view('welcome');
});

Route::get('cdm', 'CdmController@index')->name('cdm');
Route::post('cdm', 'CdmController@process')->name('cdm');
Route::get('cdm/result', 'CdmController@result')->name('cdm-result');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/account/update', 'Auth\UpdateController@index')->name('acc-update');

Route::get('linkedin', function () {
    return view('auth/loginlinkedin');
});
Auth::routes();

Route::get('/redirect', 'SocialAuthLinkedinController@redirect');
Route::get('/callback', 'SocialAuthLinkedinController@callback');

Route::get('/home', 'HomeController@index')->name('home');