<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100%;
                margin: 0;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
            .wrap{
                height: 100%;
            }
            .section{
                height: 100%;
                float: left;
                display: flex;
            }
            .section.left{
                background: #fcd700;
            }
            .text-fill{
                width: 100%;
                position: relative;
            }
            .section.left .text-fill input{
                padding: 10px 15px;
                width: calc(100% - 30px);
                background: none;
                border: none;
                text-align: center;
                font-size: 18px;
                color: black;
            }

            .section.left .text-fill input:focus{
                outline: none;
            }
            .section.left .text-fill i{
                position: absolute;
                top: 13px;
                margin-left: 10px;
                color: black;
            }
            .container{
                align-self: center;
                text-align: center;
                width: 100%;
            }

            /* Customize the label (the container) */
            .outer {
              display: block;
              position: relative;
              padding-left: 35px;
              margin-bottom: 12px;
              cursor: pointer;
              font-size: 22px;
              -webkit-user-select: none;
              -moz-user-select: none;
              -ms-user-select: none;
              user-select: none;
                margin-top: 10px;
                font-size: 16px;
                color: black;
                font-weight: bold;
                text-align: left;
            }

            /* Hide the browser's default checkbox */
            .outer input {
              position: absolute;
              opacity: 0;
              cursor: pointer;
              height: 0;
              width: 0;
            }

            /* Create a custom checkbox */
            .checkmark {
                position: absolute;
                top: 0;
                left: 0;
                height: 18px;
                width: 18px;
                border: 1px solid #000;
            }

            /* On mouse-over, add a grey background color */
            .outer:hover input ~ .checkmark {
              background-color: #ffeb79;
            }

            /* When the checkbox is checked, add a blue background */
            .outer input:checked ~ .checkmark {
              background-color: #ffffff;
            }

            /* Create the checkmark/indicator (hidden when not checked) */
            .checkmark:after {
              content: "";
              position: absolute;
              display: none;
            }

            /* Show the checkmark when checked */
            .outer input:checked ~ .checkmark:after {
              display: block;
            }

            /* Style the checkmark/indicator */
            .outer .checkmark:after {
                left: 5px;
                top: 1px;
              width: 5px;
              height: 10px;
              border: solid black;
              border-width: 0 3px 3px 0;
              -webkit-transform: rotate(45deg);
              -ms-transform: rotate(45deg);
              transform: rotate(45deg);
            }
            .reg{
                width: 450px;
                margin: 0 auto;
            }
            .btn{
                padding: 12px;
                font-size: 17px;
                border-radius: 4px;
                cursor: pointer;
                font-family: Montserrat,-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';
            }
            .btn.login{
                width: calc(100% - 30px);
                color: white;
                background: #907b00;
                border: 1px solid #000;
            }
            .btn.login:hover{
                background: black;
            }
            .btn.regis:hover{
                background: #ffe75f;
            }
            .btn.regis{
                margin-top: 20px;
                width: 100%;
                background: #fcd700;
                color: black;
                border: 1px solid #000;
            }
            .logo{
                font-family: Montserrat,-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';
                font-weight: 600;
                text-transform: uppercase;
                position: absolute;
                font-size: 45px;
                box-shadow: 2px 2px 20px 0px #0000008a;
                top: 10px;
            }
            .logo.right{
                color: black;
                left: 50%;
                padding: 5px 30px 5px 10px;
                background: #fcd700;
                z-index: 999;
                border-top-right-radius: 5px;
                border-bottom-right-radius: 5px;
            }
            .logo.left{
                color: white;
                right: 50%;
                padding: 5px 30px 5px 30px;
                background: #000000;
                border-top-left-radius: 5px;
                border-bottom-left-radius: 5px;
                z-index: 99;
            }
            .line-header{
                font-family: Montserrat,-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';
                font-size: 28px;
                color: black;
                font-weight: bold;
                text-align: left;
                margin-bottom: 20px;
                text-shadow: 0 0 13px #ffffff;
                text-transform: uppercase;
            }
            .reg-form{
                background: none;
                border: none;
                border-bottom: 1px solid black;
                padding: 10px 20px;
            }
            .reg-body input{
                width: 100%;
            }
            .container{
                height: 550px;
            }
            .btn.linkedin{
                width: calc(100% - 30px);
                border: 1px solid black;
                font-weight: bold;
                color: black;
                margin-bottom: 20px;
            }
            .btn.linkedin img{
                width: 100px;
                margin-bottom: 4px;
                margin-left: 10px;
            }
            .left .btn.linkedin:hover{
                background: #ffc107;
            }
            .right .btn.linkedin:hover{
                background: #e2e2e2;
            }
        </style>
    </head>
    <body>
        <div class="wrap">
            <div class="logo right">EUREKA !</div>
            <div class="logo left">GO</div>
            <div class="section col-md-6 left">
                <div class="container">
                    

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="reg">
                <div class="line-header">{{ __('Login') }}</div>

                <div class="reg-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <a href="{{ url('/redirect') }}" class="btn linkedin">Connect with <img src="{{ asset('img/linkedin.png') }}"></a>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 15px;">
                            <div class="col-md-12">
                                <div style="z-index: 99;
                                    position: relative;
                                    background: #fcd700;
                                    width: 40px;
                                    margin: 0 auto;
                                    font-size: 15px;
                                    font-weight: bold;
                                    color: black;">OR</div>
                                <div style="width: calc(100% - 30px);
                                border-bottom: 1px solid #e4c300;
                                position: absolute;
                                top: 10px;"></div>
                            </div>
                        </div>
                        <div class="row">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                            <div class="col-md-12 text-fill">
                                <i class="fas fa-user"></i>
                                <input id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email" style="border-bottom: 1px solid black;">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-fill">
                                <i class="fas fa-key"></i>
                                <input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 40px; margin-top: 20px;">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <label class="outer">{{ __('Remember Me') }}
                                      <input type="checkbox" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                      <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn login btn-primary">
                                    {{ __('Login') }}
                                </button>
                                <br>
                                <a class="btn btn-link" href="{{ route('password.request') }}" style="font-size: 14px; font-weight: bold; color: #907b00; margin-top: 15px; display: block;">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



                </div>
            </div>
            <div class="section col-md-6 right">
                

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="reg">
                <div class="line-header" style="text-align: right;">{{ __('Register') }}</div>

                <div class="reg-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="row">
                            <div class="col-md-12">
                                <a href="{{ url('/redirect') }}" class="btn linkedin">Join with <img src="{{ asset('img/linkedin.png') }}"></a>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 15px;">
                            <div class="col-md-12">
                                <div style="z-index: 99;
                                    position: relative;
                                    background: white;
                                    width: 40px;
                                    margin: 0 auto;
                                    font-size: 15px;
                                    font-weight: bold;
                                    color: black;">OR</div>
                                <div style="width: calc(100% - 30px);
                                border-bottom: 1px solid #dadada;
                                position: absolute;
                                top: 10px;"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <input id="first_name" type="text" class="{{ $errors->has( 'first_name' ) ? ' is-invalid' : '' }} reg-form" name="first_name" value="{{ old( 'first_name' ) }}" required autofocus placeholder="{{ __( 'First Name' ) }}">

                                @if ($errors->has( 'first_name' ))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first( 'first_name' ) }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <input id="last_name" type="text" class="{{ $errors->has( 'last_name' ) ? ' is-invalid' : '' }} reg-form" name="last_name" value="{{ old( 'last_name' ) }}" required autofocus placeholder="{{ __( 'Last Name' ) }}">

                                @if ($errors->has( 'last_name' ))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first( 'last_name' ) }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        @foreach( $inputs as $key => $row)

                            <div class="row">
                                <div class="col-md-12">
                                    <input id="{{ $key }}" type="{{ $row['type'] }}" class="{{ $errors->has( $key ) ? ' is-invalid' : '' }} reg-form" name="{{ $key }}" value="{{ old( $key ) }}" required autofocus placeholder="{{ __( $row['placeholder'] ) }}">

                                    @if ($errors->has( $key ))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first( $key ) }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                        @endforeach

                        <div class="row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn regis btn-primary" style="margin-top: 20px; width: 100%;">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

            </div>
        </div>
    </body>
</html>