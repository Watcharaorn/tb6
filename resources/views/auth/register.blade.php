@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        @foreach( $inputs as $key => $row)

                            <div class="form-group row">
                                <label for="{{ $key }}" class="col-md-4 col-form-label text-md-right">{{ __( $row['name'] ) }}</label>

                                <div class="col-md-6">
                                    <input id="{{ $key }}" type="{{ $row['type'] }}" class="form-control{{ $errors->has( $key ) ? ' is-invalid' : '' }}" name="{{ $key }}" value="{{ old( $key ) }}" required autofocus>

                                    @if ($errors->has( $key ))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first( $key ) }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                        @endforeach

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
