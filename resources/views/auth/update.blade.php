<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fcd700;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100%;
                margin: 0;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
            .wrap{
                height: 100%;
            }

            .text-fill{
                width: 100%;
                position: relative;
            }
            .fill { 
                min-height: 100%;
                height: 100%;
            }

            /* Customize the label (the container) */
            .outer {
              display: block;
              position: relative;
              padding-left: 35px;
              margin-bottom: 12px;
              cursor: pointer;
              font-size: 22px;
              -webkit-user-select: none;
              -moz-user-select: none;
              -ms-user-select: none;
              user-select: none;
                margin-top: 10px;
                font-size: 16px;
                color: black;
                font-weight: bold;
                text-align: left;
            }

            /* Hide the browser's default checkbox */
            .outer input {
              position: absolute;
              opacity: 0;
              cursor: pointer;
              height: 0;
              width: 0;
            }

            /* Create a custom checkbox */
            .checkmark {
                position: absolute;
                top: 0;
                left: 0;
                height: 18px;
                width: 18px;
                border: 1px solid #000;
            }

            /* On mouse-over, add a grey background color */
            .outer:hover input ~ .checkmark {
              background-color: #ffeb79;
            }

            /* When the checkbox is checked, add a blue background */
            .outer input:checked ~ .checkmark {
              background-color: #ffffff;
            }

            /* Create the checkmark/indicator (hidden when not checked) */
            .checkmark:after {
              content: "";
              position: absolute;
              display: none;
            }

            /* Show the checkmark when checked */
            .outer input:checked ~ .checkmark:after {
              display: block;
            }

            /* Style the checkmark/indicator */
            .outer .checkmark:after {
                left: 5px;
                top: 1px;
              width: 5px;
              height: 10px;
              border: solid black;
              border-width: 0 3px 3px 0;
              -webkit-transform: rotate(45deg);
              -ms-transform: rotate(45deg);
              transform: rotate(45deg);
            }
            .reg{
                width: 450px;
                margin: 0 auto;
            }
            .btn{
                padding: 12px;
                font-size: 17px;
                border-radius: 4px;
                cursor: pointer;
                font-family: Montserrat,-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';
            }
            .btn.login{
                width: calc(100% - 30px);
                color: white;
                background: #907b00;
                border: 1px solid #000;
            }
            .btn.login:hover{
                background: black;
            }
            .btn.regis:hover{
                background: #ffe75f;
            }
            .btn.regis{
                margin-top: 20px;
                width: 100%;
                background: #fcd700;
                color: black;
                border: 1px solid #000;
            }
            .logo{
                font-family: Montserrat,-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';
                font-weight: 600;
                text-transform: uppercase;
                position: absolute;
                font-size: 45px;
                box-shadow: 2px 2px 20px 0px #0000008a;
                top: 10px;
            }
            .logo.right{
                color: black;
                left: 50%;
                padding: 5px 30px 5px 10px;
                background: #fcd700;
                z-index: 999;
                border-top-right-radius: 5px;
                border-bottom-right-radius: 5px;
            }
            .logo.left{
                color: white;
                right: 50%;
                padding: 5px 30px 5px 30px;
                background: #000000;
                border-top-left-radius: 5px;
                border-bottom-left-radius: 5px;
                z-index: 99;
            }
            .line-header{
                font-family: Montserrat,-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';
                font-size: 28px;
                color: black;
                font-weight: bold;
                text-align: left;
                margin-bottom: 20px;
                text-shadow: 0 0 13px #ffffff;
                text-transform: uppercase;
            }
            .reg-form{
                background: none;
                border: none;
                border-bottom: 1px solid black;
                padding: 10px 20px;
            }
            .reg-body input{
                width: 100%;
            }
            .btn.linkedin{
                width: calc(100% - 30px);
                border: 1px solid black;
                font-weight: bold;
                color: black;
                margin-bottom: 20px;
            }
            .btn.linkedin img{
                width: 100px;
                margin-bottom: 4px;
                margin-left: 10px;
            }
            .left .btn.linkedin:hover{
                background: #ffc107;
            }
            .right .btn.linkedin:hover{
                background: #e2e2e2;
            }
            .avatar{
                background-image: url('https://scontent.fbkk2-2.fna.fbcdn.net/v/t1.0-9/18342655_1046467028786674_278224603151287270_n.jpg?_nc_cat=106&oh=d901fe734e7dfa4ae137e450ba97121f&oe=5C196217');
                height: 200px;
                width: 200px;
                background-size: 100%;
                background-position: center;
                border-radius: 100%;
            }
        </style>
    </head>
    <body>
        <div class="wrap">
<!--             <div class="logo right">EUREKA !</div>
            <div class="logo left">GO</div> -->
            <div class="section fill">
                <div class="container fill">
                    <div class="row fill">
                        <div class="col-md-8 offset-md-2 fill" style="    background: white;
                            padding: 0;
                            box-shadow: 0 0 20px 0px #9c9c9c;">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="avatar"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </body>
</html>