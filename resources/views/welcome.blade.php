<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100%;
                margin: 0;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
            .wrap{
                height: 100%;
            }
            .section{
                width: 50%;
                height: 100%;
                float: left;
                display: flex;
            }
            .section.left{
                background: #fcd700;
            }
            .container{
                align-self: center;
                text-align: center;
                width: 100%;
            }
        </style>
    </head>
    <body>
        <div class="wrap">
            <div class="section left">
                <div class="container">Login</div>
            </div>
            <div class="section right">RIGHT</div>
        </div>
    </body>
</html>
