<head>
  <!-- Plotly.js -->
  <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
</head>

<body>
  
  <div id="myDiv" style="width: 800px; height: 800px;"><!-- Plotly chart will be drawn inside this DIV --></div>
  <div>
      Top 1: {{ $data['sub_rank'][0][0] }} , {{ $data['sub_rank'][0][1] }} <br>
      Top 2: {{ $data['sub_rank'][1][0] }} , {{ $data['sub_rank'][1][1] }} <br><br>
      Bottom 1: {{ $data['sub_rank'][7][0] }} , {{ $data['sub_rank'][7][1] }} <br>
      Bottom 2: {{ $data['sub_rank'][6][0] }} , {{ $data['sub_rank'][6][1] }}
  </div>

  <div>
  </div>
  <script>
    <!-- JAVASCRIPT CODE GOES HERE -->
  </script>
</body>

<script type="text/javascript">
var trace1 = {
  x: [ {{ $data['xy'][0] }} ],
  y: [ {{ $data['xy'][1] }} ],
  mode: 'markers+text',
  type: 'scatter',
  name: 'Team A',
  text: ['X:{{ $data['xy'][0] }} , Y:{{ $data['xy'][1] }}'],
  textposition: 'top center',
  textfont: {
    family:  'Raleway, sans-serif'
  },
  marker: { size: 16 }
};

var trace2 = {
  x: [1, 2, 3, 4, 5],
  y: [1, 6, 3, 6, 1],
  mode: 'markers+text',
  type: 'scatter',
  name: 'Team A',
  text: ['A-1', 'A-2', 'A-3', 'A-4', 'A-5'],
  textposition: 'top center',
  textfont: {
    family:  'Raleway, sans-serif'
  },
  marker: { size: 12 }
};

var data = [ trace1 ];

var layout = { 
  xaxis: {
    range: [-9, 9] 
  },
  yaxis: {
    range: [-9, 9]
  },
  title:'Data Labels Hover'
};

Plotly.newPlot('myDiv', data, layout);

</script>