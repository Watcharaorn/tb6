<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous">
</script>

<!-- Styles -->
<link href="https://fonts.googleapis.com/css?family=Prompt:300,400,400i,500,600,700&amp;subset=thai" rel="stylesheet">
<link href="{{ asset('css/cdm.css') }}" rel="stylesheet">
<script type="text/javascript" src="{{ asset('js/owl.carousel.min.js') }}"></script>


<header>
	<div class="container">	
		<a href="http://eurekahire.com/" target="_blank" class="logo">
			eureka
		</a>
	</div>
</header>
<div class="box-board">	
	<div class="container">		
		<input type="hidden" id="game_mode" value="move">
		<!-- <div class="head">
			<h1>
				Title
			</h1>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.
			</p>
		</div> -->
		<div class="board">
			<div class="title-board">
				<strong>LABEL</strong>
				<img src="{{ asset('img/cdm/emo-p3.png') }}" class="emo">
				<p>
					Pick <b>1</b> pos-it to this board. 
				</p>
			</div>
			<div class="area-card">	
				<div class="space-card" data-limit="0" id="tb-n3">
					<div class="tb-sp-add"></div>
					<div class="tb-sp-msg"></div>
					<div class="label">
						Most Undesireable <img src="{{ asset('img/cdm/emo-n3.png') }}" class="emo">
					</div>
				</div>
				<div class="space-card" data-limit="0" id="tb-n2">
					<div class="tb-sp-add"></div>
					<div class="tb-sp-msg"></div>
					<div class="label">
						Undesireable <img src="{{ asset('img/cdm/emo-n2.png') }}" class="emo">
					</div>
				</div>
				<div class="space-card" data-limit="0" id="tb-n1">
					<div class="tb-sp-add"></div>
					<div class="tb-sp-msg"></div>
					<div class="label">
						Somewhat Undesireable <img src="{{ asset('img/cdm/emo-n1.png') }}" class="emo">
					</div>
				</div>

				<div class="space-card" data-limit="0" id="tb-p0">					
					<div class="tb-sp-add"></div>
					<div class="tb-sp-msg"></div>
					<div class="label">
						Normal <img src="{{ asset('img/cdm/emo-0.png') }}" class="emo">
					</div>
				</div>

				<div class="space-card" data-limit="0" id="tb-p1">
					<div class="tb-sp-add"></div>
					<div class="tb-sp-msg"></div>
					<div class="label">
						Somewhat Prefered<img src="{{ asset('img/cdm/emo-p2.png') }}" class="emo">
					</div>
				</div>
				<div class="space-card" data-limit="0" id="tb-p2">
					<div class="tb-sp-add"></div>
					<div class="tb-sp-msg"></div>
					<div class="label">
						Prefered <img src="{{ asset('img/cdm/emo-p2.png') }}" class="emo">
					</div>
				</div>
				<div class="space-card" data-limit="0" id="tb-p3">
					<div class="tb-sp-add"></div>
					<div class="tb-sp-msg"></div>
					<div class="label">
						Most Prefered <img src="{{ asset('img/cdm/emo-p3.png') }}" class="emo">
					</div>
				</div>
			</div>
			<a id="next-step" class="btn-confirm"></a>			
			<form action="{{ route('cdm') }}" method="post" enctype="multipart/form-data">
			    {{ csrf_field() }}
				<div id="logged">
					<input type="hidden" id="final_result" name="result" value="">
					<input type="hidden" name="log_cdm[]" value="begin">
					<input type="submit" name="submit" class="btn-summit" value="">
				</div>
			</form>
		</div>
	</div>
</div>
<div class="box-card">
	<div class="container" id="tb-hand"></div>
</div>
<div class="popup">
	<div class="mesk"></div>
	<div class="inner-popup">
		<a class="btn-close">✖</a>
		<div class="box-cordition">
			<div class="condition-1">
				<h1>
					condition 1
				</h1>
				<p>
					คลิกเลือกโพสอิทจากด้านล่าง<br>
					แล้วคลิกกรอบสีขาวบนบอร์ดเพื่อเลือกโพสอิทนั้นติดบอร์ด
				</p>
			</div>
			<div class="condition-2">
				<h1>
					condition 2
				</h1>
				<p>
					คุณสามารถสลับโพสอิทคำตอบได้ โดยคลิกที่การ์ดที่ต้องการสลับ<br>
					สลับได้ทั้งจากโพสอิทด้านล่างกับโพสอิทบนบอร์ดที่เลือกแล้ว

				</p>
			</div>
		</div>
	</div>
</div>
@foreach($deck as $key => $cards)
	<div class="space-card hidden {{$key}}">
	@foreach( $cards as $card )
		<div id="{{ $card['var'] }}" class="tb-card {{ $key }} {{ $card['axist'] }}">
			{{ $card['statement'] }}
		</div>
	@endforeach
	</div>
@endforeach
<script type="text/javascript">
	var body     = $("body"),
		popup 	 = $(".popup"),
		condition = $(".box-cordition > div");

	$(".popup .btn-close").click(function(){
		body.removeClass("open-popup");			
		setTimeout(function(){ 
			condition.hide();
		}, 400);
		
	});
	function openPopup(id){		
		$(".condition-"+id).show();
		setTimeout(function(){ 
			body.addClass("open-popup");
		}, 400);
	}
	// openPopup(1);

	currentPhase = 0;
	result = [];
	confirmStep();
	addCardScript( $('.tb-card') );

    $('#inputid').bind('keypress', function(e) {
        if (e.which == 32){//space bar
            $('#next-step').press();
        }
	});

	$('#next-step').click( confirmStep );

	function addLog($action,$from,$to)
	{
		var message = $action+','+$($from).attr('id')+','+$($to).attr('id')+','+jQuery.now();
		$('#logged').append('<input type="hidden" name="log_cdm[]" value="'+message+'">');
	}

	function createResult()
	{
		var result = '';
		$('.space-card').each( function(){
			if( $(this).attr('id') != null && $(this).attr('id') != 'tb-hand'){
				result = result + $(this).attr('id') + '_';
				$(this).find('.tb-card').each( function(){
					result = result + $(this).attr('id') + ',';
				} );
				result = result + '|';
			}
		});
		$('#final_result').val(result);
		$('.btn-summit').show();
	}

	function confirmStep(){
		addLog('confirm',currentPhase,currentPhase + 1)
		preProcess(currentPhase);
		currentPhase++;
		switchPhase(currentPhase);
		$(this).hide();
		verifyNextStep();
	}

	function phaseClean()
	{
        $('.tb-card').removeClass('active');
        $('.space-card').removeClass('active');
        $('.tb-card').removeClass('picked');
        $('.space-card').data('pick',0);
        $('.space-card').data('limit',0);
        $('.tb-sp-msg').hide();
        paireSort( $('#tb-hand') );
	}

	function paireSort($space)
	{
		range = $space.find('.tb-card').length;

		for (i = 0; i < range; i++) { 
			$thisEle = $space.find('.tb-card').eq(i);
		    if( i % 2 == 0 ){
		    	if( !$thisEle.hasClass('Y') )
		    	{
		    		if( findNextClass('Y', $thisEle) ){
		    			findNextClass('Y', $thisEle).insertBefore( $thisEle );
		    		}
		    	}
		    }else{
		    	if( !$thisEle.hasClass('X') )
		    	{
		    		if( findNextClass('X', $thisEle) ){
		    			findNextClass('X', $thisEle).insertBefore( $thisEle );
		    		}
		    	}
		    }
		}
	}

	function findNextClass($class, $space)
	{
		if( $space.next().length == 0 ){

			return false;
		}
		if( $space.hasClass($class) )
		{
			return $space;
		}
	
		return findNextClass($class,$space.next());
	}

	function isPaired($space)
	{
		is_paired = true;
		odd	= null;
		even = null;
		$space.find('.tb-card').each(function(index){
			if( index % 2 == 0 ){
				if( $(this).hasClass('X') ){
					odd = 'X';
				}else{
					odd = 'Y';
				}
			}else{
				if( $(this).hasClass('X') ){
					even = 'X';
				}else{
					even = 'Y';
				}
				if( odd == even ){
					is_paired = false;
					return false; 
				}
			}
		});
		return is_paired;
	}

	function calculateLimit()
	{
		is_complete = 1;
		$('.space-card').each( function()
		{
			limit = $(this).data('limit');
			cards = $(this).find( ".tb-card" );
			addBtn = $(this).find('.tb-sp-add');
			game_mode = $('#game_mode').val();
			if(game_mode == 'move'){
				if(limit == null || limit <= cards.length)
				{
					addBtn.removeClass('active');
				}else{
					addBtn.addClass('active');
				}
				addBtn.html('<p>ADD ' + (limit - cards.length)+'</p>' );
			}else{
				addBtn.removeClass('active');
				$('.space-card').each(function(){
					if ( $(this).data('pick') > 0 ){
						$(this).find('.tb-sp-msg').show();
						$(this).find('.tb-sp-msg').html('Please select ' + $(this).data('pick') +' more pos-it(s)' );
					}
				});
			}
		});
		verifyNextStep();
	}


	$('.tb-sp-add').click(function(){
		pickedCard = $('.tb-card.picked');
		if( pickedCard.length != 0 ){
			addLog('move',pickedCard, $(this).parent() );
			$(this).parent().append(pickedCard);
			pickedCard.removeClass('picked');
			calculateLimit();
		}
		verifyNextStep();
	});

	function addCardScript(div){
		div.click(function(){
			game_mode = $('#game_mode').val();
			pickedCard = $('.tb-card.picked');
			if ( $(this).parent("#tb-hand").length && pickedCard.length )
			{
				pickedCard.removeClass('picked');
				$(this).addClass('picked');
			}else if (game_mode == 'move'){
				if( pickedCard.length == 0 ){
					$(this).addClass('picked');
				}else if( $(this).attr('id') == pickedCard.attr('id') ){
					$(this).removeClass('picked');
				}else if( $(this).attr('id') != pickedCard.attr('id') ){
					addLog('swap',$(this),pickedCard);

					pickedCard.removeClass('picked');
					cloneSorc = $(this).clone();
					cloneDest = pickedCard.clone();
					addCardScript(cloneSorc);
					addCardScript(cloneDest);

					$(this).replaceWith(cloneDest);
					pickedCard.replaceWith(cloneSorc);
				}
			}else{
				section = $(this).parent();
				limit   = section.data('pick');
				picked  = section.find('.tb-card.picked').length;
				if( $(this).hasClass('picked') ){
					$(this).removeClass('picked');
				}else if( limit > picked && limit > 0 ){
					$(this).addClass('picked');
				}
				picked  = section.find('.tb-card.picked').length;
				pickMore = limit - (picked);
				section.find('.tb-sp-msg').html('Please select ' + pickMore +' more pos-it(s)' );
			}
			verifyNextStep();
		});
	}


////////// Stage ///////////
	function preProcess($phase)
	{
		switch($phase) {
			case 0:
				toHand = $('.space-card.TI-1 .tb-card');
				$('#tb-hand').append(toHand);
				break;
		    case 4:
		    	$('#tb-p0').append($('#tb-hand .tb-card'));
		        toHand = $('.space-card.TI-2 .tb-card');
				$('#tb-hand').append(toHand);
				break;
		    case 8:
		    	$('#tb-p0').append($('#tb-hand .tb-card'));
		        toHand = $('.space-card.AS-1 .tb-card');
				$('#tb-hand').append(toHand);
				break;
		    case 12:
		    	$('#tb-p0').append($('#tb-hand .tb-card'));
		        toHand = $('.space-card.AS-2 .tb-card');
				$('#tb-hand').append(toHand);
				break;
		    case 16:
		    	$('#tb-p0').append($('#tb-hand .tb-card'));
		    	$('#tb-hand').append($('#tb-p3 .tb-card'));
		        break;
		    case 17:
		    	$('#tb-hand').append($('#tb-p2 .tb-card'));
		        break;
		    case 18:
		    	$('#tb-p1').append($('#tb-hand .tb-card'));
		    	paireSort( $('#tb-p1') );
		        break;
		    case 20:
		    	$('#tb-hand').append($('#tb-n3 .tb-card'));
		    	break;
		    case 21:
		    	$('#tb-hand').append($('#tb-n2 .tb-card'));
		        break;
		    case 22:
		    	$('#tb-n1').append($('#tb-hand .tb-card'));
		    	paireSort( $('#tb-n1') );
		        break;
		    case 24:
		    	$('#tb-hand').append($('#tb-p0 .tb-card'));
		    	paireSort( $('#tb-hand') );
		        break;
		    case 25:
		    	$('#tb-p0').append($('#tb-hand .tb-card'));
		        break;
		}
	}

	function switchPhase($phase)
	{
		var title = $(".title-board");

		console.log("case " + $phase);
		switch($phase) {

		    case 1: // Pick Most Like
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
				$('#tb-p3').addClass('active');
		        $('#tb-p3').data('limit',1);
		        $('#game_mode').val('move');
		        title.html('<strong>Most Prefered</strong>'+
		        	'<img src="{{ asset("img/cdm/emo-p3.png") }}" class="emo">'+
		        	'<p>Pick <b>1</b> pos-it that explain the most about you.</p>');      
		        break;
		    case 2: // Pick Most Dislike
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
				$('#tb-n3').addClass('active');
		        $('#tb-n3').data('limit',1);
		        $('#game_mode').val('move');
		        title.html('<strong>Most Undesireable</strong>'+
		        	'<img src="{{ asset("img/cdm/emo-n3.png") }}" class="emo">'+
		        	'<p>Pick <b>1</b> pos-it to this board. The best answer on the top.</p>');      
		        break;
		    case 3: // Pick 2 Likes
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
		        $('#tb-p2').addClass('active');
		        $('#tb-p2').data('limit',2);
		        $('#game_mode').val('move');
		        title.html('<strong>Prefered</strong>'+
		        	'<img src="{{ asset("img/cdm/emo-p2.png") }}" class="emo">'+
		        	'<p>Pick <b>2</b> pos-it to this board. The best answer on the top.</p>');      
		        break;
		    case 4: // Pick 2 Dislike
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
		        $('#tb-n2').addClass('active');
		        $('#tb-n2').data('limit',2);
		        $('#game_mode').val('move');
		        title.html('<strong>Undesireable</strong>'+
		        	'<img src="{{ asset("img/cdm/emo-n2.png") }}" class="emo">'+
		        	'<p>Pick <b>2</b> pos-it to this board. The best answer on the top.</p>');   
		        break;
		    case 5: // Pick Most Like
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
				$('#tb-p3').addClass('active');
		        $('#tb-p3').data('limit',2);
		        $('#game_mode').val('move');
		        title.html('<strong>Most Prefered</strong>'+
		        	'<img src="{{ asset("img/cdm/emo-p3.png") }}" class="emo">'+
		        	'<p>Pick <b>2</b> pos-it to this board. The best answer on the top.</p>');  
		        break;
		    case 6: // Pick Most Dislike
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
				$('#tb-n3').addClass('active');
		        $('#tb-n3').data('limit',2);
		        $('#game_mode').val('move');
		        title.html('<strong>Most Undesireable</strong>'+
		        	'<img src="{{ asset("img/cdm/emo-n3.png") }}" class="emo">'+
		        	'<p>Pick <b>2</b> pos-it to this board. The best answer on the top.</p>'); 
		        break;
		    case 7: // Pick 2 Likes
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
		        $('#tb-p2').addClass('active');
		        $('#tb-p2').data('limit',4);
		        $('#game_mode').val('move');
		        title.html('<strong>Prefered</strong>'+
		        	'<img src="{{ asset("img/cdm/emo-p2.png") }}" class="emo">'+
		        	'<p>Pick <b>4</b> pos-it to this board. The best answer on the top.</p>');  
		        break;
		    case 8: // Pick 2 Dislike
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
		        $('#tb-n2').addClass('active');
		        $('#tb-n2').data('limit',4);
		        $('#game_mode').val('move');
		        title.html('<strong>Undesireable</strong>'+
		        	'<img src="{{ asset("img/cdm/emo-n2.png") }}" class="emo">'+
		        	'<p>Pick <b>4</b> pos-it to this board. The best answer on the top.</p>');   
		        break;
		    case 9: // Pick Most Like
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
				$('#tb-p3').addClass('active');
		        $('#tb-p3').data('limit',3);
		        $('#game_mode').val('move');
		        title.html('<strong>Most Prefered</strong>'+
		        	'<img src="{{ asset("img/cdm/emo-p3.png") }}" class="emo">'+
		        	'<p>Pick <b>3</b> pos-it to this board. The best answer on the top.</p>');  
		        break;
		    case 10: // Pick Most Dislike
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
				$('#tb-n3').addClass('active');
		        $('#tb-n3').data('limit',3);
		        $('#game_mode').val('move');
		        title.html('<strong>Most Undesireable</strong>'+
		        	'<img src="{{ asset("img/cdm/emo-n3.png") }}" class="emo">'+
		        	'<p>Pick <b>3</b> pos-it to this board. The best answer on the top.</p>');  
		        break;
		    case 11: // Pick 2 Likes
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
		        $('#tb-p2').addClass('active');
		        $('#tb-p2').data('limit',6);
		        $('#game_mode').val('move');
		        $('#game_mode').val('move');
		        title.html('<strong>Prefered</strong>'+
		        	'<img src="{{ asset("img/cdm/emo-p2.png") }}" class="emo">'+
		        	'<p>Pick <b>6</b> pos-it to this board. The best answer on the top.</p>');  
		        break;
		    case 12: // Pick 2 Dislike
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
		        $('#tb-n2').addClass('active');
		        $('#tb-n2').data('limit',6);
		        $('#game_mode').val('move');
		        title.html('<strong>Undesireable</strong>'+
		        	'<img src="{{ asset("img/cdm/emo-n2.png") }}" class="emo">'+
		        	'<p>Pick <b>6</b> pos-it to this board. The best answer on the top.</p>');  
		        break;
		    case 13: // Pick Most Like
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
				$('#tb-p3').addClass('active');
		        $('#tb-p3').data('limit',4);
		        $('#game_mode').val('move');
		        title.html('<strong>Most Prefered</strong>'+
		        	'<img src="{{ asset("img/cdm/emo-p3.png") }}" class="emo">'+
		        	'<p>Pick <b>4</b> pos-it to this board. The best answer on the top.</p>'); 
		        break;
		    case 14: // Pick Most Dislike
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
				$('#tb-n3').addClass('active');
		        $('#tb-n3').data('limit',4);
		        $('#game_mode').val('move');
		        title.html('<strong>Most Undesireable</strong>'+
		        	'<img src="{{ asset("img/cdm/emo-n3.png") }}" class="emo">'+
		        	'<p>Pick <b>4</b> pos-it to this board. The best answer on the top.</p>');  
		        break;
		    case 15: // Pick 2 Likes
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
		        $('#tb-p2').addClass('active');
		        $('#tb-p2').data('limit',8);
		        $('#game_mode').val('move');
		        title.html('<strong>Prefered</strong>'+
		        	'<img src="{{ asset("img/cdm/emo-p2.png") }}" class="emo">'+
		        	'<p>Pick <b>2</b> pos-it to this board. The best answer on the top.</p>');  
		        break;
		    case 16: // Pick 2 Dislike
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
		        $('#tb-n2').addClass('active');
		        $('#tb-n2').data('limit',8);
		        $('#game_mode').val('move');
		        title.html('<strong>Undesireable</strong>'+
		        	'<img src="{{ asset("img/cdm/emo-n2.png") }}" class="emo">'+
		        	'<p>Pick <b>2</b> pos-it to this board. The best answer on the top.</p>');  
		        break;

		    case 17: // Pick 2 Most Like
		    	phaseClean();
		        $('#tb-hand .tb-pos-it').addClass('active');
		        $('#tb-p3').addClass('active');
		        $('#tb-p3').data('limit',2);
		        $('#game_mode').val('move');
		        title.html('<strong>Most Prefered</strong>'+
		        	'<img src="{{ asset("img/cdm/emo-p3.png") }}" class="emo">'+
		        	'<p>Pick <b>2</b> pos-it to this board. The best answer on the top.</p>');   
		        openPopup(3);
		        break;
		    case 18: // Pick 4 Most Like
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
		        $('#tb-p2').addClass('active');
		        $('#tb-p2').data('limit',4);
		        $('#game_mode').val('move');
		        title.html('<strong>Prefered</strong>'+
		        	'<img src="{{ asset("img/cdm/emo-p2.png") }}" class="emo">'+
		        	'<p>Pick <b>4</b> pos-it to this board. The best answer on the top.</p>');  
		        break;
		    case 19: // Swap 6
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
		        $('#tb-p1').addClass('active');
		        $('#tb-p1 .tb-card').addClass('active');
		        $('#tb-p1').data('limit',6);
		        $('#game_mode').val('move');
		        $(".area-card").addClass("isSmall");
		        title.html('<strong>Swap Positive 1</strong>'+
		        	'<img src="{{ asset("img/cdm/emo-p1.png") }}" class="emo">'+
		        	'<p>Swap the pos-its in this board. The best answer on the top.</p>');  
		        break;
		    case 20: // Swap 6
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
		        $('#tb-p1').addClass('active');
		        $('#tb-p2').addClass('active');
		        $('#tb-p3').addClass('active');
		        $('#tb-p1 .tb-card').addClass('active');
		        $('#tb-p2 .tb-card').addClass('active');
		        $('#tb-p3 .tb-card').addClass('active');
		        $('#tb-p1').data('limit',6);
		        $('#tb-p2').data('limit',4);
		        $('#tb-p3').data('limit',2);
		        $('#game_mode').val('move');
		        title.html('<strong>Swap Positive</strong>'+
		        	'<p>Swap the pos-its in this board. The best answer on the top.</p>'); 
		        break;

		    case 21: // Pick 2 Most Like
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
		        $('#tb-n3').addClass('active');
		        $('#tb-n3').data('limit',2);
		        $('#game_mode').val('move');
		        $(".area-card").removeClass("isSmall");
		        title.html('<strong>Most Undesireable</strong>'+
		        	'<img src="{{ asset("img/cdm/emo-n3.png") }}" class="emo">'+
		        	'<p>Pick <b>2</b> pos-it to this board. The best answer on the top.</p>');  
		        break;
		    case 22: // Pick 4 Most Like
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
		        $('#tb-n2').addClass('active');
		        $('#tb-n2').data('limit',4);
		        $('#game_mode').val('move');
		        title.html('<strong>Undesireable</strong>'+
		        	'<img src="{{ asset("img/cdm/emo-n2.png") }}" class="emo">'+
		        	'<p>Pick <b>2</b> pos-it to this board. The best answer on the top.</p>');  
		        break;
		    case 23: // Swap 6
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
		        $('#tb-n1').addClass('active');
		        $('#tb-n1 .tb-card').addClass('active');
		        $('#tb-n1').data('limit',6);
		        $('#game_mode').val('move');
		        $(".area-card").addClass("isSmall");
		        title.html('<strong>Swap Somewhat Undesireable</strong>'+
		        	'<img src="{{ asset("img/cdm/emo-n1.png") }}" class="emo">'+
		        	'<p>Swap the pos-its in this board. The best answer on the top.</p>'); 
		        break;
		    case 24: // Swap 6
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
		        $('#tb-n1').addClass('active');
		        $('#tb-n2').addClass('active');
		        $('#tb-n3').addClass('active');
		        $('#tb-n1 .tb-card').addClass('active');
		        $('#tb-n2 .tb-card').addClass('active');
		        $('#tb-n3 .tb-card').addClass('active');
		        $('#tb-n1').data('limit',6);
		        $('#tb-n2').data('limit',4);
		        $('#tb-n3').data('limit',2);
		        $('#game_mode').val('move');
		        title.html('<strong>Swap Negative</strong>'+
		        	'<p>Swap the pos-its in this board. The best answer on the top.</p>'); 
		        break;
		    case 25: // Swap 6
		    	phaseClean();
		        $('#tb-hand .tb-card').addClass('active');
		        $('#tb-n1').addClass('active');
		        $('#tb-p1').addClass('active');
		        $('#tb-n1 .tb-card').addClass('active');
		        $('#tb-p1 .tb-card').addClass('active');
		        $('#tb-n1').data('limit',6);
		        $('#tb-p1').data('limit',6);
		        $('#game_mode').val('move');
		        $('#game_mode').val('move');
		        title.html('<strong>Swap Somewhat Undesireable VS Positive 1</strong>'+
		        	'<p>Swap the pos-its in this board. The best answer on the top.</p>'); 
		        break;
		    default:// Thank You
		    	phaseClean();
		    	$(".area-card").append("<div class='thank'>Thank You</div>");
		    	createResult();
		    	break;
		}
		calculateLimit();
	}

	function verifyNextStep()
	{
		switch(currentPhase) {
			case 1: 
		    case 2:
		    case 3:
		    case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			case 10:
			case 11:
			case 12:
			case 13:
			case 14:
			case 15:
			case 16:
				// Move
				if( $('.tb-sp-add').length == $('.tb-sp-add').not('.active').length ){ $('#next-step').show(); }else{ $('#next-step').hide(); }
				break;
			case 17:
				// Move & Paired
				if( $('.tb-sp-add').length == $('.tb-sp-add').not('.active').length ){ 
					if( isPaired( $('#tb-p3') )){ 
						$('#next-step').show(); 
					}else{ 
						$('#next-step').hide(); 
					}
				}
				break;
			case 18:
				// Move & Paired
				if( $('.tb-sp-add').length == $('.tb-sp-add').not('.active').length ){ 
					if( isPaired( $('#tb-p2') )){ 
						$('#next-step').show(); 
					}else{ 
						$('#next-step').hide(); 
					}
				}
				break;
			case 19:
				// Move & Paired
				if( $('.tb-sp-add').length == $('.tb-sp-add').not('.active').length ){ 
					if( isPaired( $('#tb-p1') )){ 
						$('#next-step').show(); 
					}else{ 
						$('#next-step').hide(); 
					}
				}
				break;
			case 20:
				// Move & Paired
				if( $('.tb-sp-add').length == $('.tb-sp-add').not('.active').length ){ 
					if( isPaired( $('#tb-p1') ) && isPaired( $('#tb-p2') ) && isPaired( $('#tb-p3') )){ 
						$('#next-step').show(); 
					}else{ 
						$('#next-step').hide(); 
					}
				}
				break;
			case 21:
				// Move & Paired
				if( $('.tb-sp-add').length == $('.tb-sp-add').not('.active').length ){ 
					if( isPaired( $('#tb-n3') )){ 
						$('#next-step').show(); 
					}else{ 
						$('#next-step').hide(); 
					}
				}
				break;
			case 22:
				// Move & Paired
				if( $('.tb-sp-add').length == $('.tb-sp-add').not('.active').length ){ 
					if( isPaired( $('#tb-n2') )){ 
						$('#next-step').show(); 
					}else{ 
						$('#next-step').hide(); 
					}
				}
				break;
			case 23:
				// Move & Paired
				if( $('.tb-sp-add').length == $('.tb-sp-add').not('.active').length ){ 
					if( isPaired( $('#tb-n1') )){ 
						$('#next-step').show(); 
					}else{ 
						$('#next-step').hide(); 
					}
				}
				break;
			case 24:
				// Move & Paired
				if( $('.tb-sp-add').length == $('.tb-sp-add').not('.active').length ){ 
					if( isPaired( $('#tb-n1') ) && isPaired( $('#tb-n2') ) && isPaired( $('#tb-n3') )){ 
						$('#next-step').show(); 
					}else{ 
						$('#next-step').hide(); 
					}
				}
				break;
			case 25:
				// Move & Paired
				if( $('.tb-sp-add').length == $('.tb-sp-add').not('.active').length ){ 
					if( isPaired( $('#tb-p1') ) && isPaired( $('#tb-p2') ) && isPaired( $('#tb-p3') ) &&
					  	isPaired( $('#tb-n1') ) && isPaired( $('#tb-n2') ) && isPaired( $('#tb-n3') )){ 
						$('#next-step').show(); 
					}else{ 
						$('#next-step').hide(); 
					}
				}
				break;

			default:
				return false;
				break;
		}
		return true;
	}
	
</script>