<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function showLoginForm()
        {
            $title = 'Login';
            $inputs = [ 'email' => [
                            'name'          => 'Email',
                            'type'          => 'text',
                            'placeholder'   => 'Email'
                            ],
                        'password' => [
                            'name'          => 'Password',
                            'type'          => 'password',
                            'placeholder'   => 'Password'
                            ],
                        'confirm_password' => [
                            'name'          => 'Confirm Password',
                            'type'          => 'password',
                            'placeholder'   => 'Confirm Password'
                            ] ];

            return view('auth.login', ['title' => $title , 'inputs' => $inputs]);
        }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
