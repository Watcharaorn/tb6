<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class UpdateController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function index()
    {
        $title = 'Register';
        $inputs = [ 'first_name' => [
                        'name'          => 'First Name',
                        'type'          => 'text',
                        'placeholder'   => ''
                        ],
                    'last_name' => [
                        'name'          => 'Last Name',
                        'type'          => 'text',
                        'placeholder'   => ''
                        ],
                    'email' => [
                        'name'          => 'Email',
                        'type'          => 'text',
                        'placeholder'   => ''
                        ],
                    'password' => [
                        'name'          => 'Password',
                        'type'          => 'text',
                        'placeholder'   => ''
                        ],
                    'confirm_password' => [
                        'name'          => 'Confirm Password',
                        'type'          => 'text',
                        'placeholder'   => ''
                        ],
                    'phone' => [
                        'name'          => 'Phone Number',
                        'type'          => 'text',
                        'placeholder'   => ''
                        ],
                    'post_code' => [
                        'name'          => 'Post Code',
                        'type'          => 'text',
                        'placeholder'   => ''
                        ],
                    'career_level' => [
                        'name'          => 'Career',
                        'type'          => 'text',
                        'placeholder'   => ''
                        ],
                    'salary' => [
                        'name'          => 'Current Salary',
                        'type'          => 'text',
                        'placeholder'   => ''
                        ],
                    'job_function' => [
                        'name'          => 'Current Job Function',
                        'type'          => 'text',
                        'placeholder'   => ''
                        ],
                    'birth_date' => [
                        'name'          => 'Birth Date',
                        'type'          => 'text',
                        'placeholder'   => ''
                        ],
                    'nationallty' => [
                        'name'          => 'Nationallity',
                        'type'          => 'text',
                        'placeholder'   => ''
                        ] ];

        return view('auth.update', ['title' => $title , 'inputs' => $inputs]);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
}
