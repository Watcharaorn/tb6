<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use App\CdmCard;
use App\CdmResult;
use App\User;

class CdmController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	$cards = CdmCard::all()->toArray();

    	$deck = [];
    	$TI_1 = [];
    	$TI_2 = [];
    	$AS_1 = [];
    	$AS_2 = [];

    	foreach( $cards as $card )
    	{
    		$num	= substr($card['var'],1);
    		unset($card['created_at']);
    		unset($card['updated_at']);

            if( $request->get('lang') == 'th' )
            {
                $card['statement'] = $card['statement_th'];
            }
            
    		if( $card['axist'] == 'Y' )
    		switch ($num) {
    			case '1':
    			case '2':
                case '5':
                case '6':
    				array_push($TI_1, $card);
    				break;
                case '3':
                case '4':
    			case '7':
    			case '8':
    				array_push($TI_2, $card);
    				break;
    		}

    		if( $card['axist'] == 'X' )
    		switch ($num) {
    			case '1':
    			case '2':
                case '5':
                case '6':
    				array_push($AS_1, $card);
    				break;
                case '3':
                case '4':
    			case '7':
    			case '8':
    				array_push($AS_2, $card);
    				break;
    		}
    	}
    	$deck['TI-1'] = $TI_1;
    	$deck['TI-2'] = $TI_2;
    	$deck['AS-1'] = $AS_1;
    	$deck['AS-2'] = $AS_2;

        return view('cdm',['deck' => $deck]);
    }

    public function process(Request $request)
    {
        $user = User::find(1);
        $cdm  = new CdmResult;
        $result = [];
        $raw = explode('|',$request->get('result'));
        $log = $request->get('log_cdm');
        foreach ($raw as $each) {

            if( empty($each) ){ continue; }

            $data = explode('_',$each);
            $val  = explode(',', rtrim($data[1],", "));

            switch ($data[0]) {
                case 'tb-p3':
                    $result['P3'] = $val;
                    break;
                case 'tb-p2':
                    $result['P2'] = $val;
                    break;
                case 'tb-p1':
                    $result['P1'] = $val;
                    break;
                case 'tb-p0':
                    $result['P0'] = $val;
                    break;
                case 'tb-n1':
                    $result['N1'] = $val;
                    break;
                case 'tb-n2':
                    $result['N2'] = $val;
                    break;
                case 'tb-n3':
                    $result['N3'] = $val;
                    break;
            }
        }

        $result = json_encode($result);
        $log    = json_encode($log);

        $cdm->result    = $result;
        $cdm->log       = $log;
        $cdm->user_id   = $user->id;
        $cdm->save();

        return redirect('cdm/result?l='.$cdm->id);
    }

    public function result(Request $request)
    {
        $client = new Client();
        $post_data['id'] = $request->get('l');
        $res = $client->request('POST', 'https://cdmeureka.azurewebsites.net/', [
            'form_params' => $post_data
        ]);

        if( $res->getStatusCode() == 200 || $res->getStatusCode() == 201 ){
            $respon = $res->getBody();
            $data = json_decode($respon, true);

        }else{
            $txt = "Cannot connect to API server.";
        }
        return view('cdm_result', ['data' => $data['items'][0]]);
    }
}
